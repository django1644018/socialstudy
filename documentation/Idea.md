## Core Idea:
* The main point of this application or the idea is to combine the technological marvels to help students to study in a very well instructed manner.
* There are going to be many features in this application like student interaction, study materials for subjects, practise grounds, roadmaps, teacher assistance, etc.
* The features are:
    * Study materials
    * Study guidance
    * Social media like sharing achievements
    * Roadmaps
* This is going to be a freemium based model where the content will be free but the assistance will be cost.
* There will be technologies involved in this project like:
    * AI Assistance
    * Encryptions
    * Web Technologies
    * Mobile application
* This is just an blue print where the actual idea will be still but there will be modifications.

## Generalized Idea:
* The concept of the user management plays an important role here where the types of data we are going to use is going to be huge.
* The relationship between the datas are the important one like user account details and the user activity details.
* Data model of the choice should be wise so that there is no problem in the implementation. Like for the user models we can use the table data structure but for the activities we should use the key value pair.