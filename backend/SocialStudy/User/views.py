from rest_framework.views import APIView
from rest_framework.response import Response
import jwt
from rest_framework import parsers
from .models import UserModel
from SocialStudy.settings import SECRET_KEY
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import AllowAny
from .serializer import UserModelSerializer
from .utils import main,tokenize,check_token,decrypt

class UserCreation(APIView):
    permission_classes = [AllowAny]
    authentication_classes = []
    def post(self,request,format=None):
        request_data = request.data
        username = request_data['username']
        password = request_data['user_password']
        serializer = UserModelSerializer(data=request_data)
        if serializer.is_valid(raise_exception=serializer.error_messages):
                serializer.save()
        token = tokenize(username,serializer.data['user_password'])
        return Response({"Token":token,"Message":"User Created Successfully"})

class UserLogin(APIView):
    permission_classes = [AllowAny]
    authentication_classes = []
    def post(self,requests,format=None):
        try:
            request_data = requests.data 
            username = request_data['username']
            password = request_data['user_password']
            user = UserModel.objects.get(username=username)
            user_password = decrypt(user.user_password)
            if password == user_password:
                return Response({"Token":tokenize(username,password)})
            else:
                return Response({"Exceptions":"Password not correct"})
        except Exception as e:
            return Response({"Exceptions":str(e)})
        
    
class Testing(APIView):
    parser_classes = [parsers.MultiPartParser,parsers.FormParser]

    def get(self,requests):
        uploaded_file = requests.FILES['jenkins.sh']
        file_content = uploaded_file.read()
        print(file_content)
        main()
        return Response({"file_content":file_content.decode()})
    
    
user_registration_view = UserCreation.as_view()
user_login_view = UserLogin.as_view()
user_test_view = Testing.as_view()