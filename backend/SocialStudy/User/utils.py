import jwt
from SocialStudy.settings import SECRET_KEY
from .models import UserModel
from cryptography.fernet import Fernet
import hashlib,base64
from rest_framework import exceptions
import datetime
def tokenize(username,user_password):
    token_expiry_time = (datetime.datetime.now()+datetime.timedelta(hours=24)).isoformat()
    payload = {
        "username":username,
        "user_password":user_password,
        "expiry_time":token_expiry_time
    }
    token = jwt.encode(payload=payload,key=SECRET_KEY,algorithm="HS256")
    return token
def check_token(token):
    try:
        payload = jwt.decode(token,key=SECRET_KEY,algorithms="HS256")
        username = payload['username']
        password = payload['user_password']
        expiry_time = payload['expiry_time']
        if not expiry_time or datetime.datetime.fromisoformat(expiry_time) < datetime.datetime.now():
            return exceptions.AuthenticationFailed({"Error":'Token expired create a new one',"username":None})
        try:
            user = UserModel.objects.get(username=username)
            userpassword = decrypt(user.user_password)
            if password == userpassword:
                return {"Error":None,"username":username}
            else:
                print(userpassword,password)
                return exceptions.AuthenticationFailed({"Error":"UserPassword not match","username":None})
        except Exception as e:
            return exceptions.AuthenticationFailed({"Error":"The token is not valid {e}","username":None})
    except Exception as e:
        return exceptions.AuthenticationFailed({"Error":'Token not valid '+str(e),"username":None})
def secret_key_retrieval():
    secret_key = SECRET_KEY.encode('utf-8')
    digest = hashlib.sha256(secret_key).digest() 
    fernet_key = base64.urlsafe_b64encode(digest)
    return fernet_key
def encrypt(password):
    secret_key = secret_key_retrieval()
    cipher_suite = Fernet(secret_key)
    password = password.encode('utf-8')
    cipher_text = cipher_suite.encrypt(password)
    return cipher_text.decode('utf-8')
def decrypt(encrypted_password):
    secret_key = secret_key_retrieval()
    cipher_suite = Fernet(secret_key)
    encrypted_password = encrypted_password.encode('utf-8')
    plain_text = cipher_suite.decrypt(encrypted_password)
    return plain_text.decode("utf-8")
def main():
    x = "hello"
    encrypted = encrypt(x)
    print(encrypted)
    print(decrypt(encrypted))