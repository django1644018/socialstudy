from .models import UserModel
from rest_framework import authentication
from rest_framework import exceptions
from .utils import check_token
from rest_framework.response import Response
class ExampleAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        try:
            token = request.headers['Token']
            token_output = check_token(token)
            user = UserModel.objects.get(username=token_output['username'])
            return (user,None)
        except Exception as e:
            raise exceptions.AuthenticationFailed({"Error":"The error is token not provided or token not valid and the error may be: "+str(e)})