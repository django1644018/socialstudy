from rest_framework import serializers
from .models import UserModel
from .utils import encrypt

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ['username','user_password','user_email','user_age','user_proffession','user_organization','user_friends_number']
    def save(self,**kwargs):
        raw_password = self.validated_data.get("user_password")
        encrypted_password = encrypt(raw_password)
        self.validated_data['user_password'] = encrypted_password
        return super().save(**kwargs)