from django.db import models
from django.contrib.auth.models import AbstractUser
class UserModel(AbstractUser):
    username = models.TextField(unique=True)
    user_password = models.TextField()
    user_email = models.EmailField(unique=True)
    user_age = models.IntegerField(null=True,default=0)
    user_proffession = models.TextField(null=True,default="None")
    user_organization = models.TextField(null=True,default="None")
    user_friends_number = models.IntegerField(null=True,default=0)
    
    REQUIRED_FIELDS = [
        "user_password"
    ]
    