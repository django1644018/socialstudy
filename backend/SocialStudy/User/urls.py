from django.urls import path
from .views import user_login_view,user_test_view,user_registration_view

urlpatterns = [
    path('user/login/',user_login_view),
    path('testing/',user_test_view),
    path('user/register/',user_registration_view)
]
