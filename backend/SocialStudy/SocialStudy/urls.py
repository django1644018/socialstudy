from django.contrib import admin
from django.urls import path,include
from User import urls as user_urls
from . import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('test/', views.send_some_data),
    path('',include(user_urls))
]
